package types

type Bitmask8 uint8
type Bitmask16 uint16
type Bitmask32 uint32
type Bitmask64 uint64
