package util_test

import (
	"fmt"
	"testing"

	"bitbucket.org/lebronto_kerovol/gwsocialnetwork/v1/util"
)

func TestComputeHash(t *testing.T) {
	t.Log(fmt.Sprintf("EncodeToStringByBase64AsURLEncoding: %s", string(util.EncodeToStringByBase64AsURLEncoding(util.ComputeHash([]byte("2521612"), util.ALGORITHM_SHA1)))))
	t.Log(fmt.Sprintf("EncodeToHexString: %s", string(util.EncodeToHexString(util.ComputeHash([]byte("2521612"), util.ALGORITHM_SHA1)))))
}
