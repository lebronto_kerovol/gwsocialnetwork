package util

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"hash"

	"bitbucket.org/lebronto_kerovol/gwsocialnetwork/v1/types"

	"golang.org/x/crypto/sha3"
)

const (
	ALGORITHM_MD5 types.Bitmask16 = 1 << iota
	ALGORITHM_SHA1
	ALGORITHM_SHA224
	ALGORITHM_SHA256
	ALGORITHM_SHA384
	ALGORITHM_SHA512
	ALGORITHM_SHA3224
	ALGORITHM_SHA3256
	ALGORITHM_SHA3384
	ALGORITHM_SHA3512
)

var (
	hasherSHA1   = sha1.New()
	hasherSHA256 = sha256.New()
	hasherSHA512 = sha512.New()
)

func ComputeHash(bytes []byte, algorithm types.Bitmask16) []byte {
	var hasher hash.Hash

	switch algorithm {
	case ALGORITHM_MD5:
		hasher = md5.New()
		break
	case ALGORITHM_SHA1:
		hasher = sha1.New()
		break
	case ALGORITHM_SHA224:
		hasher = sha256.New224()
		break
	case ALGORITHM_SHA256:
		hasher = sha256.New()
		break
	case ALGORITHM_SHA384:
		hasher = sha512.New384()
		break
	case ALGORITHM_SHA512:
		hasher = sha512.New()
		break
	case ALGORITHM_SHA3224:
		hasher = sha3.New224()
		break
	case ALGORITHM_SHA3256:
		hasher = sha3.New256()
		break
	case ALGORITHM_SHA3384:
		hasher = sha3.New384()
		break
	case ALGORITHM_SHA3512:
		hasher = sha3.New512()
		break
	}

	hasher.Reset()
	hasher.Write(bytes)

	return hasher.Sum(nil)
}

func ComputeHashPtrString(str *string, algorithm types.Bitmask16) []byte {
	if str == nil {
		return []byte{}
	}

	return ComputeHash([]byte(*str), algorithm)
}

func ComputeHashString(str string, algorithm types.Bitmask16) []byte {
	return ComputeHash([]byte(str), algorithm)
}

func EncodeToStringByBase64AsURLEncoding(bytes []byte) string {
	return base64.URLEncoding.EncodeToString(bytes)
}

func EncodeToHexString(bytes []byte) string {
	return hex.EncodeToString(bytes)
}
