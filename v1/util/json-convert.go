package util

import (
	"errors"
	"strconv"
)

func FieldNumericToString(nameField string, fieldId interface{}) (string, error) {
	idInt64, ok := fieldId.(int64)
	if ok {
		return strconv.Itoa(int(idInt64)), nil
	}

	idUInt64, ok := fieldId.(uint64)
	if ok {
		return strconv.Itoa(int(idUInt64)), nil
	}

	idFloat64, ok := fieldId.(float64)
	if ok {
		return strconv.FormatFloat(idFloat64, 'f', 0, 64), nil
	}

	return ``, errors.New(`ERROR: Field "` + nameField + `" is not support parsing to string. (support only: int64, uint64, float64)`)
}