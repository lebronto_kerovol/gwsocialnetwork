package util

import (
	"encoding/json"
	"errors"

	"bitbucket.org/lebronto_kerovol/gwsocialnetwork/v1/binding"
)

func GetMapFromBytesOfMassage(bytesMessage *[]byte) (map[string]interface{}, error) {
	dict := make(map[string]interface{})

	if err := json.Unmarshal(*bytesMessage, &dict); err != nil {
		return nil, err
	}

	return dict, nil
}

func SetMapToBytesOfMassage(dict *map[string]interface{}) (*[]byte, error) {
	bytes, err := json.Marshal(dict)
	if err != nil {
		return nil, err
	}

	return &bytes, nil
}

func GetRequestKey(dict *map[string]interface{}) (*string, error) {
	key, isExist := (*dict)[binding.JSON_REQUEST_KEY].(string)
	if !isExist {
		return nil, errors.New("Request key is not Exist!")
	}

	return &key, nil
}

func SetRequestKey(requestKey string, dict *map[string]interface{}) {
	(*dict)[binding.JSON_REQUEST_KEY] = requestKey
}

func GetJSONResponseWithRequestKeyAsBytes(fromRequest *map[string]interface{}, toResponse *map[string]interface{}) ([]byte, error) {
	requestKey, err := GetRequestKey(fromRequest)
	if err != nil {
		return nil, err
	}

	SetRequestKey(*requestKey, toResponse)

	bytesResponse, err := json.Marshal(*toResponse)
	if err != nil {
		return nil, err
	}

	return bytesResponse, nil
}

func GetJSONResponseWithRequestKeyAsMap(fromRequest *map[string]interface{}, toResponse *map[string]interface{}) (*map[string]interface{}, error) {
	requestKey, err := GetRequestKey(fromRequest)
	if err != nil {
		return nil, err
	}

	SetRequestKey(*requestKey, toResponse)
	return toResponse, nil
}
