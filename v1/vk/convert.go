package vk

import (
	"errors"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/lebronto_kerovol/gwerror"
)

func BDateStringToTime(bdate string) (*time.Time, error) {
	values := strings.Split(bdate, `.`)

	if len(values) != 3 {
		return nil, gwerror.NewError(``, ``, errors.New(`ERROR: Parameter "bdate" is invalid.`))
	}

	year, err := strconv.Atoi(values[2])
	if err != nil {
		return nil, gwerror.WrapError(``, ``, errors.New(`ERROR: Field "year" is invalid.`), err)
	}

	month, err := strconv.Atoi(values[1])
	if err != nil {
		return nil, gwerror.WrapError(``, ``, errors.New(`ERROR: Field "month" is invalid.`), err)
	}

	day, err := strconv.Atoi(values[0])
	if err != nil {
		return nil, gwerror.WrapError(``, ``, errors.New(`ERROR: Field "day" is invalid.`), err)
	}

	result := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)

	return &result, nil
}
