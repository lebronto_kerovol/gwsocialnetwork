package vk

import (
	"strconv"
	"time"

	"bitbucket.org/lebronto_kerovol/gwsocialnetwork/v1/util"
)

type object struct {
	Id    *int64  `json:"id,omitempty"`
	Title *string `json:"title,omitempty"`
}

type City struct {
	object
}

type Contacts struct {
	MobilePhone *string `json:"mobile_phone,omitempty"`
	HomePhone   *string `json:"home_phone,omitempty"`
}

type Country struct {
	object
}

type LastSeen struct {
	Platform *int   `json:"platform,omitempty"`
	Time     *int64 `json:"time,omitempty"`
}

type User struct {
	/**  Идентификатор пользователя.*/
	Id *int64 `json:"id,omitempty"`
	/**  Имя.*/
	FirstName *string `json:"first_name,omitempty"`
	/**  Фамилия.*/
	LastName *string `json:"last_name,omitempty"`
	/**  Поле возвращается, если страница пользователя удалена или заблокирована, содержит значение ```deleted``` или ```banned```. В этом случае опциональные поля не возвращаются.*/
	Deactivated *string `json:"deactivated,omitempty"`
	/**  Скрыт ли профиль пользователя настройками приватности.*/
	IsClosed *bool `json:"is_closed,omitempty"`
	/**  Может ли текущий пользователь видеть профиль при ```is_closed = 1``` (например, он есть в друзьях).*/
	CanAccessClosed *bool `json:"can_access_closed,omitempty"`
	/**  Содержимое поля «О себе» из профиля.*/
	About *string `json:"about,omitempty"`
	/**  Содержимое поля «Деятельность» из профиля.*/
	Activities *string `json:"activities,omitempty"`
	/**  [bdate] Дата рождения. Возвращается в формате ```D.M.YYYY``` или ```D.M``` (если год рождения скрыт). Если дата рождения скрыта целиком, поле отсутствует в ответе.*/
	StringBirthday *string `json:"bdate,omitempty"` // time.Time
	/**  Информация о городе, указанном на странице пользователя в разделе «Контакты». Возвращаются следующие поля:
	* @response - id (integer) — идентификатор города, который можно использовать для получения его названия с помощью метода ```database.getCitiesById```;
	* - title (string) — название города.*/
	City *City `json:"city,omitempty"`
	/**  Возвращает данные об указанных в профиле сервисах пользователя, таких как: ```skype```, ```facebook```, ```twitter```, ```livejournal```, ```instagram```. Для каждого сервиса возвращается отдельное поле с типом ```string```, содержащее никнейм пользователя. Например, ```"instagram": "username"```.*/
	Connections *map[string]string `json:"connections,omitempty"`
	/**  Информация о телефонных номерах пользователя. Если данные указаны и не скрыты настройками приватности, возвращаются следующие поля:
	* @response - ```mobile_phone``` (string) — номер мобильного телефона пользователя (только для Standalone-приложений);
	* - ```home_phone``` (string) — дополнительный номер телефона пользователя.*/
	Contacts *Contacts `json:"contacts,omitempty"`
	/**  Информация о стране, указанной на странице пользователя в разделе «Контакты». Возвращаются следующие поля:
	* @response - ```id``` (integer) — идентификатор страны, который можно использовать для получения ее названия с помощью метода ```database.getCountriesById```;
	* - ```title``` (string) — название страны.*/
	Country *Country `json:"country,omitempty"`
	/**  Короткий адрес страницы. Возвращается строка, содержащая короткий адрес страницы (например, ```andrew```). Если он не назначен, возвращается ```"id"+user_id```, например, ```id35828305```.*/
	Domain *string `json:"domain,omitempty"`
	/**  Информация о том, известен ли номер мобильного телефона пользователя. Возвращаемые значения: ```1``` — известен, ```0``` — не известен.*/
	HasMobile *int `json:"has_mobile,omitempty"`
	/**  Информация о том, установил ли пользователь фотографию для профиля. Возвращаемые значения: ```1``` — установил, ```0``` — не установил.*/
	HasPhoto *int `json:"has_photo,omitempty"`
	/**  Название родного города.*/
	HomeTown *string `json:"home_town,omitempty"`
	/**  Время последнего посещения. Объект, содержащий следующие поля:
	* @response - ```time``` (integer) — время последнего посещения в формате ```Unixtime```.
	* - ```platform``` (integer) — тип платформы. Возможные значения:
	* ```
	* 1 — мобильная версия;
	* 2 — приложение для iPhone;
	* 3 — приложение для iPad;
	* 4 — приложение для Android;
	* 5 — приложение для Windows Phone;
	* 6 — приложение для Windows 10;
	* 7 — полная версия сайта.
	* ```*/
	LastSeen *LastSeen `json:"last_seen,omitempty"`
	/**  Девичья фамилия.*/
	MaidenName *string `json:"maiden_name,omitempty"`
	/**  Никнейм (отчество) пользователя.*/
	Nickname *string `json:"nickname,omitempty"`
	/**  Информация о том, находится ли пользователь сейчас на сайте. Если пользователь использует мобильное приложение либо мобильную версию, возвращается дополнительное поле ```online_mobile```, содержащее ```1```. При этом, если используется именно приложение, дополнительно возвращается поле ```online_app```, содержащее его идентификатор.*/
	Online *int `json:"online,omitempty"`
	/**  Короткое имя страницы.*/
	ScreenName *string `json:"screen_name,omitempty"`
	/**  Пол. Возможные значения:
	* ```
	* 1 — женский;
	* 2 — мужской;
	* 0 — пол не указан.
	* ```*/
	Sex *int `json:"sex,omitempty"`
	/**  Адрес сайта, указанный в профиле.*/
	Site *string `json:"site,omitempty"`
	/**  Возвращается ```1```, если страница пользователя верифицирована, ```0``` — если нет.*/
	Verified *int `json:"verified,omitempty"`
	/**  Режим стены по умолчанию. Возможные значения: ```owner```, ```all```.*/
	WallDefault *string `json:"wall_default,omitempty"`
	hashToken   *string `json:"id,omitempty"`
}

func (user *User) GetId() string {
	if user.Id != nil {
		return strconv.FormatInt(*user.Id, 10)
	}

	return ""
}

// Get SHA-3/512 hash "id" in hex-string format.
//
// (if you need hash in other algorithm, used method GetId()
// for calculating in other hash)
func (user *User) GetHashId() string {
	if user.Id != nil {
		return util.EncodeToHexString(util.ComputeHashString(strconv.FormatInt(*user.Id, 10), util.ALGORITHM_SHA3512))
	}

	return ""
}

// Is equel hash Id. (SHA-3/512 hash "id" in hex-string format)
//
// (if you need hash in other algorithm, used method GetId()
// for calculating in other hash)
func (user *User) IsEquelHashId(hashId string) bool {
	hash := user.GetHashId()

	return hash != "" && hashId != "" && hash == hashId
}

func (user *User) GetFirstName() string {
	if user.FirstName != nil {
		return *user.FirstName
	}

	return ""
}

func (user *User) GetLastName() string {
	if user.LastName != nil {
		return *user.LastName
	}

	return ""
}

func (user *User) GetBirthday() *time.Time {
	if user.StringBirthday != nil {
		bDate, err := BDateStringToTime(*user.StringBirthday)
		if err != nil {
			return nil
		}

		return bDate
	}

	return nil
}

func (user *User) GetHashToken() string {
	if user.hashToken != nil {
		return *user.hashToken
	}

	return ""
}
