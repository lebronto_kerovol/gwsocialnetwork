package vk

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/lebronto_kerovol/gwsocialnetwork/types"
	"bitbucket.org/lebronto_kerovol/gwsocialnetwork/v1/util"
)

const (
	TAG_ACCESS_TOKEN_TEXT   = `<@!TEXT_ACCESS_TOKEN_TEXT-VALIDATION-001!@>`
	TAG_REQUEST_FIELDS_TEXT = `<@!TEXT_REQUEST_FIELDS_TEXT-VALIDATION-002!@>`
)

//Get access_token: https://oauth.vk.com/authorize?client_id=5216&display=page&redirect_uri=url_redirect&response_type=token&v=5.131
const (
	//Response: {"response":{"count":1,"items":[{"type":"standalone","id":52,"title":"NameAPI","author_owner_id":6326,"is_installed":true,"webview_url":"","hide_tabbar":0,"icon_139":"url_icon","icon_150":"url_icon","icon_278":"url_icon","icon_75":"url_icon","has_vk_connect":false,"author_url":"url_author","banner_1120":"url_banner","banner_560":"url_banner","international":false,"is_in_catalog":0,"leaderboard_type":0,"members_count":6126}]}}
	urlMethodAppId = `https://api.vk.com/method/apps.get?access_token=` + TAG_ACCESS_TOKEN_TEXT + `&v=5.131`
	//Response: {"response":[{"first_name":"","id":612,"last_name":"","can_access_closed":true,"is_closed":false,"bdate":"dd.mm.yyyy"}]}
	urlMethodUserGet   = `https://api.vk.com/method/users.get?fields=` + TAG_REQUEST_FIELDS_TEXT + `&access_token=` + TAG_ACCESS_TOKEN_TEXT + `&v=5.131`
	urlMethodUserIdGet = `https://api.vk.com/method/users.get?fields=id&access_token=` + TAG_ACCESS_TOKEN_TEXT + `&v=5.131`
)

func GetAppIdByVKAccessToken(accessToken string) (*string, error) {
	replacerMainContent := strings.NewReplacer(TAG_ACCESS_TOKEN_TEXT, accessToken)
	response, err := http.Get(replacerMainContent.Replace(urlMethodAppId))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	rawResponse, err := ioutil.ReadAll(response.Body)

	dict, err := util.GetMapFromBytesOfMassage(&rawResponse)
	if err != nil {
		return nil, err
	}

	_, isExist := dict[`error`]
	if isExist {
		return nil, errors.New(fmt.Sprintf(`ERROR: Invalid request (GET). (Response: %s)`, string(rawResponse)))
	}

	resultResponse, isExist := dict[`response`]
	if !isExist {
		return nil, errors.New(`ERROR: Field "response" is not exist in JSON response from VK server.`)
	}

	fieldsResponse, ok := resultResponse.(map[string]interface{})
	if !ok {
		return nil, errors.New(`ERROR: Field type assertion failed. Collection "Map" expected. (Response)`)
	}

	responseItems, ok := fieldsResponse[`items`]
	if !ok {
		return nil, errors.New(`ERROR: Field "items" is not exist in JSON response from VK server.`)
	}

	collectionMessageResponse, ok := responseItems.([]interface{})
	if !ok {
		return nil, errors.New(`ERROR: Field type assertion failed. Collection "Array" expected. (Response)`)
	}

	if len(collectionMessageResponse) == 0 {
		return nil, errors.New(`ERROR: Collection is empty. (Response)`)
	}

	fieldsObject, ok := collectionMessageResponse[0].(map[string]interface{})
	if !ok {
		return nil, errors.New(`ERROR: Field type assertion failed. Collection "Map" expected. (Response)`)
	}

	id, isExist := fieldsObject[`id`]
	if !isExist {
		return nil, errors.New(`ERROR: Field "id" is not exist in JSON response from VK server.`)
	}

	result := strconv.FormatFloat(id.(float64), 'f', 0, 64)
	return &result, nil
}

func GetUserIdByVKAccessToken(accessToken string) (*string, error) {
	replacerMainContent := strings.NewReplacer(TAG_ACCESS_TOKEN_TEXT, accessToken)
	response, err := http.Get(replacerMainContent.Replace(urlMethodUserIdGet))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	rawResponse, err := ioutil.ReadAll(response.Body)

	dict, err := util.GetMapFromBytesOfMassage(&rawResponse)
	if err != nil {
		return nil, err
	}

	_, isExist := dict[`error`]
	if isExist {
		return nil, errors.New(fmt.Sprintf(`ERROR: Invalid request (GET). (Response: %s)`, string(rawResponse)))
	}

	resultResponse, isExist := dict[`response`]
	if !isExist {
		return nil, errors.New(`ERROR: Field "response" is not exist in JSON response from VK server.`)
	}

	collectionMessageResponse, ok := resultResponse.([]interface{})
	if !ok {
		return nil, errors.New(`ERROR: Field type assertion failed. Collection "Array" expected. (Response)`)
	}

	if len(collectionMessageResponse) == 0 {
		return nil, errors.New(`ERROR: Collection is empty. (Response)`)
	}

	fieldsObject, ok := collectionMessageResponse[0].(map[string]interface{})
	if !ok {
		return nil, errors.New(`ERROR: Field type assertion failed. Collection "Map" expected. (Response)`)
	}

	fieldId, isExist := fieldsObject[`id`]
	if !isExist {
		return nil, errors.New(`ERROR: Field "id" is not exist in JSON response from VK server.`)
	}

	id, err := util.FieldNumericToString(`id`, fieldId)
	if err != nil {
		return nil, err
	}

	return &id, nil
}

const (
	/**  Идентификатор пользователя.*/
	REQUEST_FIELD_id = "id"
	/**  Имя.*/
	REQUEST_FIELD_first_name = "first_name"
	/**  Фамилия.*/
	REQUEST_FIELD_last_name = "last_name"
	/**  Поле возвращается, если страница пользователя удалена или заблокирована, содержит значение ```deleted``` или ```banned```. В этом случае опциональные поля не возвращаются.*/
	REQUEST_FIELD_deactivated = "deactivated"
	/**  Скрыт ли профиль пользователя настройками приватности.*/
	REQUEST_FIELD_is_closed = "is_closed"
	/**  Может ли текущий пользователь видеть профиль при ```is_closed = 1``` (например, он есть в друзьях).*/
	REQUEST_FIELD_can_access_closed = "can_access_closed"
	/**  Содержимое поля «О себе» из профиля.*/
	REQUEST_FIELD_about = "about"
	/**  Содержимое поля «Деятельность» из профиля.*/
	REQUEST_FIELD_activities = "activities"
	/**  Дата рождения. Возвращается в формате ```D.M.YYYY``` или ```D.M``` (если год рождения скрыт). Если дата рождения скрыта целиком, поле отсутствует в ответе.*/
	REQUEST_FIELD_bdate = "bdate"
	/**  Информация о городе, указанном на странице пользователя в разделе «Контакты». Возвращаются следующие поля:
	* @response - id (integer) — идентификатор города, который можно использовать для получения его названия с помощью метода ```database.getCitiesById```;
	* - title (string) — название города.*/
	REQUEST_FIELD_city = "city"
	/**  Возвращает данные об указанных в профиле сервисах пользователя, таких как: ```skype```, ```facebook```, ```twitter```, ```livejournal```, ```instagram```. Для каждого сервиса возвращается отдельное поле с типом ```string```, содержащее никнейм пользователя. Например, ```"instagram": "username"```.*/
	REQUEST_FIELD_connections = "connections"
	/**  Информация о телефонных номерах пользователя. Если данные указаны и не скрыты настройками приватности, возвращаются следующие поля:
	* @response - ```mobile_phone``` (string) — номер мобильного телефона пользователя (только для Standalone-приложений);
	* - ```home_phone``` (string) — дополнительный номер телефона пользователя.*/
	REQUEST_FIELD_contacts = "contacts"
	/**  Информация о стране, указанной на странице пользователя в разделе «Контакты». Возвращаются следующие поля:
	* @response - ```id``` (integer) — идентификатор страны, который можно использовать для получения ее названия с помощью метода ```database.getCountriesById```;
	* - ```title``` (string) — название страны.*/
	REQUEST_FIELD_country = "country"
	/**  Короткий адрес страницы. Возвращается строка, содержащая короткий адрес страницы (например, ```andrew```). Если он не назначен, возвращается ```"id"+user_id```, например, ```id35828305```.*/
	REQUEST_FIELD_domain = "domain"
	/**  Информация о том, известен ли номер мобильного телефона пользователя. Возвращаемые значения: ```1``` — известен, ```0``` — не известен.*/
	REQUEST_FIELD_has_mobile = "has_mobile"
	/**  Информация о том, установил ли пользователь фотографию для профиля. Возвращаемые значения: ```1``` — установил, ```0``` — не установил.*/
	REQUEST_FIELD_has_photo = "has_photo"
	/**  Название родного города.*/
	REQUEST_FIELD_home_town = "home_town"
	/**  Время последнего посещения. Объект, содержащий следующие поля:
	* @response - ```time``` (integer) — время последнего посещения в формате ```Unixtime```.
	* - ```platform``` (integer) — тип платформы. Возможные значения:
	* ```
	* 1 — мобильная версия;
	* 2 — приложение для iPhone;
	* 3 — приложение для iPad;
	* 4 — приложение для Android;
	* 5 — приложение для Windows Phone;
	* 6 — приложение для Windows 10;
	* 7 — полная версия сайта.
	* ```*/
	REQUEST_FIELD_last_seen = "last_seen"
	/**  Девичья фамилия.*/
	REQUEST_FIELD_maiden_name = "maiden_name"
	/**  Никнейм (отчество) пользователя.*/
	REQUEST_FIELD_nickname = "nickname"
	/**  Информация о том, находится ли пользователь сейчас на сайте. Если пользователь использует мобильное приложение либо мобильную версию, возвращается дополнительное поле ```online_mobile```, содержащее ```1```. При этом, если используется именно приложение, дополнительно возвращается поле ```online_app```, содержащее его идентификатор.*/
	REQUEST_FIELD_online = "online"
	/**  Короткое имя страницы.*/
	REQUEST_FIELD_screen_name = "screen_name"
	/**  Пол. Возможные значения:
	* ```
	* 1 — женский;
	* 2 — мужской;
	* 0 — пол не указан.
	* ```*/
	REQUEST_FIELD_sex = "sex"
	/**  Адрес сайта, указанный в профиле.*/
	REQUEST_FIELD_site = "site"
	/**  Возвращается ```1```, если страница пользователя верифицирована, ```0``` — если нет.*/
	REQUEST_FIELD_verified = "verified"
	/**  Режим стены по умолчанию. Возможные значения: ```owner```, ```all```.*/
	REQUEST_FIELD_wall_default = "wall_default"
)

func getPartUrlOfRequestFields(requestFields *[]string) string {
	result := ""

	for _, field := range *requestFields {
		result += field + ","
	}

	if len(*requestFields) > 0 {
		result = result[:len(result)-1]
	}

	return result
}

func getUserByRequestFields(response *map[string]interface{}, requestFields *[]string) (types.User, error) {
	marshalJson, err := json.Marshal(response)
	if err != nil {
		return nil, err
	}

	var user User

	if err := json.Unmarshal(marshalJson, &user); err != nil {
		return nil, err
	}

	return &user, nil
}

// Get user with request fields. (default include: id)
func GetUserByVKAccessToken(accessToken string, requestFields ...string) (types.User, error) {
	replacerMainContent := strings.NewReplacer(
		TAG_REQUEST_FIELDS_TEXT, getPartUrlOfRequestFields(&requestFields),
		TAG_ACCESS_TOKEN_TEXT, accessToken,
	)
	response, err := http.Get(replacerMainContent.Replace(urlMethodUserGet))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	rawResponse, err := ioutil.ReadAll(response.Body)

	dict, err := util.GetMapFromBytesOfMassage(&rawResponse)
	if err != nil {
		return nil, err
	}

	_, isExist := dict[`error`]
	if isExist {
		return nil, errors.New(fmt.Sprintf(`ERROR: Invalid request (GET). (Response: %s)`, string(rawResponse)))
	}

	resultResponse, isExist := dict[`response`]
	if !isExist {
		return nil, errors.New(`ERROR: Field "response" is not exist in JSON response from VK server.`)
	}

	collectionMessageResponse, ok := resultResponse.([]interface{})
	if !ok {
		return nil, errors.New(`ERROR: Field type assertion failed. Collection "Array" expected. (Response)`)
	}

	if len(collectionMessageResponse) == 0 {
		return nil, errors.New(`ERROR: Collection is empty. (Response)`)
	}

	fieldsObject, ok := collectionMessageResponse[0].(map[string]interface{})
	if !ok {
		return nil, errors.New(`ERROR: Field type assertion failed. Collection "Map" expected. (Response)`)
	}

	return getUserByRequestFields(&fieldsObject, &requestFields)
}
