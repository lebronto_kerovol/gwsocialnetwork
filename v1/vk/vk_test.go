package vk_test

import (
	"fmt"
	"testing"

	"bitbucket.org/lebronto_kerovol/gwsocialnetwork/v1/vk"
)

const tokenVK = `dfedc20a02bn23832bcn23d09adee8c32n32761b495932n6e4d1df55ad885313c4c2b27ef1e71g2g1g213h3249c687970fea7524d616a1`

func TestGetAppIDByVKAccessToken(t *testing.T) {
	id, err := vk.GetAppIdByVKAccessToken(tokenVK)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(fmt.Sprintf("AppID by VK Access Token: %s", *id))
}

func TestGetUserIDByVKAccessToken(t *testing.T) {
	id, err := vk.GetUserIdByVKAccessToken(tokenVK)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(fmt.Sprintf("UserID by VK Access Token: %s", *id))
}

func TestGetUserByVKAccessToken(t *testing.T) {
	user, err := vk.GetUserByVKAccessToken(tokenVK)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(fmt.Sprintf("UserID by VK Access Token: %+v", user))
}
