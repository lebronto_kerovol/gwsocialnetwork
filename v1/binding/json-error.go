package binding

import "bitbucket.org/lebronto_kerovol/gwerror"

const (
	JSON_DATA_IS_NOT_EXIST        = "ERROR: Data is not exist!"
	JSON_ACTION_IS_NOT_EXIST      = "ERROR: Action is not exist!"
	JSON_REQUEST_KEY_IS_NOT_EXIST = "ERROR: Request key is not exist!"
)

var (
	jsonDataIsNotExist       = gwerror.NewErrorFromString(``, `binding/json/error`, JSON_DATA_IS_NOT_EXIST)
	jsonActionIsNotExist     = gwerror.NewErrorFromString(``, `binding/json/error`, JSON_ACTION_IS_NOT_EXIST)
	jsonRequestKeyIsNotExist = gwerror.NewErrorFromString(``, `binding/json/error`, JSON_REQUEST_KEY_IS_NOT_EXIST)
)

func getJsonDataIsNotExist() *gwerror.Error {
	return jsonDataIsNotExist
}

func getJsonActionIsNotExist() *gwerror.Error {
	return jsonActionIsNotExist
}

func getJsonRequestKeyIsNotExist() *gwerror.Error {
	return jsonRequestKeyIsNotExist
}
