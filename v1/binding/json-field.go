package binding

const (
	JSON_DATA        = "data"
	JSON_ACTION      = "action"
	JSON_REQUEST_KEY = "request-key"
)
