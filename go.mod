module bitbucket.org/lebronto_kerovol/gwsocialnetwork

go 1.16

require (
	bitbucket.org/lebronto_kerovol/gwerror v0.0.0-20210720211604-bf02946bdaf8
	golang.org/x/crypto v0.0.0-20210813211128-0a44fdfbc16e
)
