package types

import "time"

type User interface {
	GetId() string
	// Get SHA-3/512 hash "id" in hex-string format.
	//
	// (if you need hash in other algorithm, used method GetId()
	// for calculating in other hash)
	GetHashId() string
	IsEquelHashId(hashId string) bool
	GetFirstName() string
	GetLastName() string
	GetBirthday() *time.Time
	// Is equel hash Id. (SHA-3/512 hash "id" in hex-string format)
	//
	// (if you need hash in other algorithm, used method GetId()
	// for calculating in other hash)
	GetHashToken() string
}
